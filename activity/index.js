// Writing comments in Js*/
// - one line comment - (ctrl + /)
/* Multi-line comments   -  (ctrl + shift + /) */


// Statement
console.log("Hello Batch 144!"); 

/*
	Javascript - we can see or log message in our console.

	Browser console are part of our be=rowsers which will allow us to see/log message data or information from aou programming language - javascrip.

	For most browsers, consoles are easily accessed through its developer tools in the console tab

	In fact, consoole vbrowser will allow us to add some javascript expressions.

	Statements 

	Statement are intsruction, expressiomn that we add to our programming language to communicate with our computers.

	JS statement usaually ends in a semi colon (;),  however, JS has implemented a way to automatically add semi colons at the end of a line/statement. semi colon can actually be omitted in creating JS statement but semicolon in JS are addded to mark the end of the statement.
	
	Synax in programming is a set of rules that descrribe how staement are properly made/constructed.

	Lines/blocks of code must follow a certain of rules for it to work properly.
	
	
*/

console.log("adriann");

/*Mini activity 
	
	create three console logs to display the following message:

	console.log("favoriteFOod"); 
	*/


/*console.log("Sinigang");
console.log("Sinigang");
console.log("Sinigang");*/

let food1= "Sinigang";



console.log(food1);


/*
	variable is a way to store information or data within our JS.

	To create a variable we first declare the name of variable with either the let/const keyword:

		let nameOfVariable
	
	Then, we can initialize the variable with a value or data.

		let nameOfVariable = <data>
*/

console.log("My favorite food is: " + food1);

let food2 = "Adobo";

console.log("My favorite food are: " + food1 + " and " + food2);

/*
	we can create a variables without and an initial value, however, the variables content is undefined.
*/


food3 = "Fried Chicken";

/*

	we ccan update the content of a let variable by reassigningthe content using an assignment operator (=);

	asignment operator (=) = let us assign data to a variable.
*/


console.log(food3)

/*
	mini activity


*/

// to update variable (no let)
food1 = "Chicken Inasal";
food2 = "Lechon"

/*
	We can update our variable with an asignment operator without to use let keyword.

	we cannot create another variable with tha same name. It will result in an error.

*/


console.log(food1);
console.log(food2);


// conts keyword
/*
	const - will also allows us to create variable. however, with a const keyword we can create constant variable, which means these data to saved in a constant will not change, cannot be change and should not be changed.

*/

const pi = 3.1416;

console.log(pi);


// Tryiing to reassigned a const variable will result to error.

/*pi = "Pizza";

console.log(pi)*/


// We also cannot declare/create a const variable without initializzzation or an initial value.
/*const gravity;

console.log(gravity);*/

let myName;
const sunRiseDirection = "East";
const sunSetDirection = "West";

/*
  You can actually log multiple items, variable, data in console.log sepearated by comma(,).
*/
console.log(myName,sunRiseDirection, sunSetDirection);

/*
	Guides in Creating a JS variable:

	1. we can create a let variable with the let keyword. let variable can be reassigned but not declared.

	2.creating a variable has two parts: declaration of the variiable name and initialization of the initial value of he variable using an assignment operator (=).

	3. A let variable can be declared without initialization. However the value of the variabble will be undefined until its re-assigned with a value.

	4. Not defined vs Undefined

		Not defined Error - means the variable is used but NOT declared

		Undefined Error - results froom a variable that is used but is noot initialized.

	5. We can use const key word  to create constant variables. Constant variables cannot be declared without initialization.
	Constant cvarianble cannot be re-assigned.

	6. When creating variablenames, start with small caps, this is because of avoiding conflict with syntax in JS that is named with capital letters.

	7. If the variables would need two woords, the nnaming convention is the use of CamelCase. Do not use add a space for variable with words as names.
*/

/*Data Types""*/

/*
	string are data type which are alphanumerical text, It could be a name, a phrase or even a sentence, We can create a String with single quote('') or double ("").

	variable = "StringData"; 
*/

console.log("Sample String Data");

let country = "Phillipines";
let province = "Rizal"

console.log(province, country);

/*
	concatenation 

		is a way for us to add strings together and have a single string. we can use our "+" symbol for this.
*/

let fullAddress = province + ',' +country; 

console.log(fullAddress);

let	greeting = "I Love in " + country;

console.log(greeting);

/*
	In JS, when you use the + sign with strings we have concatenation
*/

let numString = "50";
let numString2 = "25";


console.log(numString + numString2);

/* 

    string have property call .length and it tells us the number of characters in a string

	Space, commas, period can also be characters when include in a string.

	It will returnn a number type data.

*/

let hero = "Cpatain America";

console.log(hero.length);


/* NUMBER TYPE*/

/*

	Number type data can actually be used in proper mathematical equation and operation.



*/
let student = 16;
console.log(student);

let num1 = 50;
let num2 = 25;

/*

	 Addition operator willl allow us to add two number type annd retur the suum as a number data type.

	 Operation  return a value can be saved in a variable.

	 What iif we use the addition operator on a number type and numerical string?

		It result to concatenation.

	parse


*/


console.log(num1 + num2);

let sum1 = num1 + num2

console.log(sum1);



let numString3 = "100";

let sum2 = parseInt(numString3) + num1

console.log(sum2);

/*mini activity*/

let sum3 = sum1 + sum2;
let sum4 = parseInt(numString2) + num2;

console.log(sum3 , sum4)


/*
	Subtraction operator

	will let us get the difference between two number types, It will also result to a proper number type data.

	When subtraction operator is used on a string and a number, the string will be coonverted into a number autoomatically and the JS will perform the operation

	This automatic conversion from one type to anotjher is call type convertion or type coercion or forced coercion.

	When a text string is subtracted with a number, It will resulte in NAN or not a number because JS converted the text to string it resultes to NaN and NaN- number =NuN

*/

let difference = num1 - num2;
console.log(difference);


let difference2 = numString3 - num2;
console.log(difference2);

let difference3 = hero - num2;
console.log(difference3);

let string1 = "fifteen";
console.log(num2 - string1);


// Multiplication Operator (*)

let num3 = 10;
let num4 = 5;

let product = num3 * num4;
console.log(product);

let product2 = numString3 * num3;
console.log(product2);

let product3 = numString * numString3
console.log(product3);

// Division Operator (/)
let num5 = 30;
let num6 = 3;
let quotient = num5 / num6;
console.log(quotient);

let quotient2 = numString3 / 5;
console.log(quotient2);


let quotient3 = 450 / num4;
console.log(quotient3);


// Boolean (True or False)
/*
	Boolean is usually used for logical operatoin or for if-else coondition.

	Naming convevntion for a variable that contains boolean is a yes or no question.
*/


let isAdmin = true;
let isMarried = false;

/*
	You can actually name your variable any way you want however, as thee best practice it should

		appropriate
		definitive of the value iit contain
		semantically correct


 let pikachu = "Tee Jae Calinao";
 console.log(pikachu);

 */

/*
 Arrays

	Arrays are a speciall kind of data type wherein we can store multile values.

 	An array can store multuple values and even of different types. for best practice, keep the data type of items in a arrays uniform.

	Vallues in a array are separated by comma. falling to do so, will result in an error

	an array is created with an array literal([])

	array indices are markers of the order oof the items in the array. Array starts at 0
*/

let koponanNiEugene = ["Eugene", "Alfred" ,"Dennis", "Vincent"];
console.log(koponanNiEugene);

// To access an array item: arrayName[index#]
console.log(koponanNiEugene[0]);

// Bad Practiice for an array:
let array2 = ["One Punch Man", true, 500, "Saitama"];
console.log(array2);


// Objects
/*
	Objects are another kind  of special data type used to mimic real wworld objects.
	With this we can add information that make sense, thematically relevant and of differennt data types.

	Object can be created with object Literal ({}).

	Each value is given a llabel which makes the date significant and meaningful this pairing of data and "label" is what we call key-value pair.

	A key-value pair is called a property.

	Each property separated by a comma (,).
*/

let person1 = {

	heroName: "One Punch Man",
	isRegistedred:true,
	salary: 500,
	realName: "Saitama"
};

// To get a value of an objects prperty, we can access it using doot motion.

// ObjectName.proopertyName
console.log(person1.realName);


// Mini Activity

let myFavoriteBands = ["Parokya Ni Edgar", "Rivermaya", "Eraserheads", "Kamikaze"];
console.log(myFavoriteBands);

let me = {

	firstName: "Adriann",
	lastname: "Furigay",
	isWebDeveloper: true,
	hasPortfolio: true,
	age: 26
};
console.log(me);


// 
/*
	Undefined vs Null

		Null is the explicit declaration that there is no value.

		Undefined means that the variable exist however a value was not initialized with  the variable.
		
*/
// Null
let sampleNull = null;

// Undefined
let undefinedSample;
console.log(undefinedSample)

// Certain processes in programming explicity returns null to indicate that the task resulted to nothing.

let foundResult = null;

// For undefines, this is normally caused by developers creatinh variable that have no value or data associated with them.

// The variable does exist but its value is still unknown.

let person2 = {
	name: "Patricia",
	age: 28
};

// // because the variable person2 does notexist however the property isAdmin does not exist.
// console.log(person1.isAdmin);



