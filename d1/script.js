// alert('working');

// Functions

function printStar() {
	console.log("*")
	console.log("**")
	console.log("***")
}

function sayHello(name) {
	// alert("Hello");
	console.log("Hello"  + name)
}

// sayHello(6)

// Function that accept two numbers and prinnt sum.

// Parameters
// function addSum(x, y) {
// 	let sum = x + y
// 	console.log(sum)
// }

// // Arguments
// addSum(13,2); 
// addSum(23, 56);




// Function with 3 parameters
// String Template Literals
// Interpolation
function printBio(lname, fname, age) {
	// console.log('Hello' + lname+' ' + fname+' ' + age);

	// to add spaces
	console.log(`Hello Mr. ${lname} ${fname} ${age}`);
}

// printBio('furigay', 'adriann', 26)



// Return Keyword
function addSum(x,y) {
	return y-x
}

let sumNum = addSum(3,4);


// Activity

function getBio(lname, fname, email, mobileNum) {
	console.log(`Hello! I am ${lname} ${fname}`)
	console.log(`You can contact me via ${email}`)
	console.log(`Mobile Number ${mobileNum}`)
}

getBio('furigay', 'adriann', 'adriann.furigay@gmail.com', '09206580304')